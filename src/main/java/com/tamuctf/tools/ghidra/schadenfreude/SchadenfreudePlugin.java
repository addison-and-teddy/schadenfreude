package com.tamuctf.tools.ghidra.schadenfreude;

import com.tamuctf.tools.ghidra.schadenfreude.decomp.SchadenfreudeDecompiler;
import ghidra.app.events.ProgramClosedPluginEvent;
import ghidra.app.events.ProgramOpenedPluginEvent;
import ghidra.framework.plugintool.Plugin;
import ghidra.framework.plugintool.PluginEvent;
import ghidra.framework.plugintool.PluginInfo;
import ghidra.framework.plugintool.PluginTool;
import ghidra.framework.plugintool.util.PluginStatus;

@PluginInfo(category = "Schadenfreude",
        description = "A collection of plugins which automate parts of the vulnerability research process, focusing mostly on data flow analysis.",
        packageName = "Schadenfreude",
        shortDescription = "Ghidra analysis plugins for enjoying the unfortunate.",
        status = PluginStatus.UNSTABLE,
        eventsConsumed = {
                ProgramOpenedPluginEvent.class, ProgramClosedPluginEvent.class
        },
        servicesProvided = {SchadenfreudeDecompiler.class})
public class SchadenfreudePlugin extends Plugin {

    private final SchadenfreudeDecompiler decompiler;

    public SchadenfreudePlugin(PluginTool tool) {
        super(tool);
        decompiler = SchadenfreudeDecompiler.getInstance();
        super.registerServiceProvided(SchadenfreudeDecompiler.class, decompiler);
    }

    @Override
    public void processEvent(PluginEvent event) {
        if (event instanceof ProgramOpenedPluginEvent) {
            decompiler.openProgram(((ProgramOpenedPluginEvent) event).getProgram());
        } else if (event instanceof ProgramClosedPluginEvent) {
            try {
                decompiler.closeProgram(((ProgramClosedPluginEvent) event).getProgram());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void dispose() {
        try {
            SchadenfreudeDecompiler.getInstance().dispose();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
