package com.tamuctf.tools.ghidra.schadenfreude.util;

import ghidra.util.task.CancelledListener;
import ghidra.util.task.TaskMonitor;
import ghidra.util.task.TaskMonitorAdapter;
import lombok.EqualsAndHashCode;
import lombok.Value;

@Value
@EqualsAndHashCode(callSuper = false)
public class TaskMonitorWrapper extends TaskMonitorAdapter {

    TaskMonitor monitor;

    @Override
    public synchronized boolean isCancelEnabled() {
        return monitor.isCancelEnabled();
    }

    @Override
    public synchronized void setCancelEnabled(boolean enable) {
        monitor.setCancelEnabled(enable);
    }

    @Override
    public void cancel() {
        monitor.cancel();
    }

    @Override
    public void clearCanceled() {
        monitor.clearCanceled();
    }

    @Override
    public synchronized void addCancelledListener(CancelledListener listener) {
        monitor.addCancelledListener(listener);
    }

    @Override
    public synchronized void removeCancelledListener(CancelledListener listener) {
        monitor.removeCancelledListener(listener);
    }

    @Override
    protected synchronized void notifyChangeListeners() {
        // nope
    }

}
