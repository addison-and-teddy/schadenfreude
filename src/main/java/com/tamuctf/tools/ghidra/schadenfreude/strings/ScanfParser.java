package com.tamuctf.tools.ghidra.schadenfreude.strings;

import com.google.common.base.Strings;
import com.tamuctf.tools.ghidra.schadenfreude.strings.format.FormatString;
import com.tamuctf.tools.ghidra.schadenfreude.strings.format.LengthSpecifier;
import com.tamuctf.tools.ghidra.schadenfreude.strings.format.ScanfEntry;
import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;

import java.util.OptionalLong;
import java.util.regex.MatchResult;
import java.util.regex.Pattern;

@UtilityClass
public class ScanfParser {

    private final Pattern VALID = Pattern
            .compile("^(?:(%)(\\*?)([0-9]*)((?:hh|h|l|ll|j|z|t|L)?)([iduoxnfegacsp])|(?:[^%]|%%)*)*$");
    private final Pattern PATTERN = Pattern
            .compile("(?:(%)(\\*?)([0-9]*)((?:hh|h|l|ll|j|z|t|L)?)([iduoxnfegacsp])|(?:[^%]|%%)*)");

    public @NotNull ScanfFormatString parseFormatString(@NotNull CharSequence format) throws IllegalArgumentException {
        if (VALID.matcher(format).matches()) {
            ScanfEntry[] scanfEntries = PATTERN.matcher(format).results().map(ScanfParser::fromMatch)
                    .toArray(ScanfEntry[]::new);
            return new ScanfFormatString(format, scanfEntries);
        }
        throw new IllegalArgumentException("Invalid scanf format string.");
    }

    private ScanfEntry fromMatch(MatchResult result) throws IllegalArgumentException {
        if (result.group(1) == null) {
            String actual = result.group().replaceAll("%%", "%");
            return new ScanfEntry(false, OptionalLong.of(actual.length()), null, ScanfEntry.Type.RAW, actual);
        } else {
            ScanfEntry.Type type = ScanfEntry.Type.getByRepresentative(result.group(5).charAt(0));
            boolean ignored = !Strings.isNullOrEmpty(result.group(2));
            OptionalLong width;
            if (Strings.isNullOrEmpty(result.group(3))) {
                width = OptionalLong.empty();
            } else {
                width = OptionalLong.of(Long.parseLong(result.group(3)));
            }
            LengthSpecifier length = type.getLengthSpecifier(result.group(4));
            return new ScanfEntry(ignored, width, length, type, null);
        }
    }

    @SuppressWarnings("RedundantModifiersUtilityClassLombok")
    public static class ScanfFormatString extends FormatString<ScanfEntry, ScanfEntry.Type> {
        public ScanfFormatString(CharSequence unprocessed, ScanfEntry[] entries) {
            super(unprocessed, entries);
        }
    }

}
