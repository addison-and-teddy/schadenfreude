package com.tamuctf.tools.ghidra.schadenfreude.decomp;

import ghidra.app.decompiler.DecompileOptions;
import ghidra.app.decompiler.DecompileResults;
import ghidra.program.model.listing.Function;
import ghidra.program.model.listing.Program;
import ghidra.util.task.TaskMonitor;
import lombok.Getter;
import lombok.Setter;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;
import java.util.function.Consumer;

public final class SchadenfreudeDecompiler {
    private static SchadenfreudeDecompiler INSTANCE = null;
    private final Map<Program, ConcurrentDecompiler> decompilers;
    private final DecompileOptions decOptions;
    private ExecutorService service;
    private @Getter int threads;
    private @Getter @Setter int maxTimeout;

    public SchadenfreudeDecompiler(int threads, int maxTimeout) {
        this.service = null;
        this.threads = threads;
        this.maxTimeout = maxTimeout;
        this.decOptions = new DecompileOptions();
        this.decOptions.setEliminateUnreachable(false);
        this.decompilers = new ConcurrentHashMap<>();
        this.service = Executors.newWorkStealingPool(threads);
    }

    public static synchronized SchadenfreudeDecompiler getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new SchadenfreudeDecompiler(Runtime.getRuntime().availableProcessors(), 15);
        }
        return INSTANCE;
    }

    public synchronized void reinitialise(int newThreads) throws InterruptedException {
        List<Runnable> needingProcessing;
        if (service != null) {
            needingProcessing = service.shutdownNow();
            service.awaitTermination(maxTimeout * 2, TimeUnit.SECONDS);
        } else {
            needingProcessing = Collections.emptyList();
        }
        this.threads = newThreads;
        service = Executors.newWorkStealingPool(threads);
        needingProcessing.forEach(service::submit);
        for (ConcurrentDecompiler cd : this.decompilers.values()) {
            cd.resize(this.threads);
        }
    }

    public synchronized void openProgram(Program program) {
        decompilers.computeIfAbsent(program,
                p -> new ConcurrentDecompiler(this.getMaxTimeout(), this.threads, decOptions, p));
    }

    public synchronized void closeProgram(Program program) throws InterruptedException {
        ConcurrentDecompiler decompiler = decompilers.remove(program);
        decompiler.dispose();
    }

    public Future<DecompileResults> decompileFunction(Function function, int timeout, TaskMonitor monitor) {
        return decompileFunction(function, timeout, monitor, false);
    }

    public Future<DecompileResults> decompileFunction(Function function, int timeout, TaskMonitor monitor,
            boolean refresh) {
        final int usedTimeout;
        if (timeout > maxTimeout) {
            usedTimeout = maxTimeout;
        } else {
            usedTimeout = timeout;
        }
        ConcurrentDecompiler decompiler = decompilers.get(function.getProgram());
        if (decompiler == null) {
            return null;
        }
        return service.submit(() -> decompiler.decompileFunction(function, usedTimeout, monitor, refresh));
    }

    public void decompileFunction(Function function, int timeout, TaskMonitor monitor,
            Consumer<DecompileResults> callback) {
        decompileFunction(function, timeout, monitor, false, callback);
    }

    public void decompileFunction(Function function, int timeout, TaskMonitor monitor, boolean refresh,
            Consumer<DecompileResults> callback) {
        final int usedTimeout;
        if (timeout > maxTimeout) {
            usedTimeout = maxTimeout;
        } else {
            usedTimeout = timeout;
        }
        ConcurrentDecompiler decompiler = decompilers.get(function.getProgram());
        if (decompiler == null) {
            callback.accept(null);
        } else {
            service.submit(() -> {
                DecompileResults results;
                try {
                    results = decompiler.decompileFunction(function, usedTimeout, monitor, refresh);
                } catch (Throwable t) {
                    results = null;
                }
                callback.accept(results);
            });
        }
    }

    public void flushCaches() {
        this.decompilers.values().forEach(ConcurrentDecompiler::flushCache);
    }

    public synchronized void dispose() throws InterruptedException {
        for (ConcurrentDecompiler cd : this.decompilers.values()) {
            cd.dispose();
        }
        this.decompilers.clear();
    }

}
