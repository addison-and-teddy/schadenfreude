package com.tamuctf.tools.ghidra.schadenfreude.strings.format;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Value;
import lombok.experimental.FieldDefaults;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.OptionalLong;

import static com.tamuctf.tools.ghidra.schadenfreude.strings.format.LengthSpecifier.*;

@Value
@AllArgsConstructor
public class ScanfEntry implements FormatStringEntry<ScanfEntry.Type> {

    boolean ignored;
    @SuppressWarnings("OptionalUsedAsFieldOrParameterType") OptionalLong width;
    LengthSpecifier length;
    @NotNull Type type;
    @Nullable String content;

    @FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
    public enum Type {
        RAW,
        INTEGER('i', 'd', 'u', 'o', 'x', 'n'),
        FLOAT('f', 'e', 'g', 'a'),
        CHARACTER('c'),
        STRING('s'),
        POINTER('p');

        char[] representatives;

        Type(char... representatives) {
            this.representatives = representatives;
        }

        @Contract(pure = true)
        public static @NotNull Type getByRepresentative(char supposed) {
            for (Type type : Type.values()) {
                for (char representative : type.representatives) {
                    if (representative == supposed) {
                        return type;
                    }
                }
            }
            throw new IllegalStateException("Illegal character used for type specifier.");
        }

        @Contract(pure = true)
        public @Nullable LengthSpecifier getLengthSpecifier(String specifier) throws IllegalArgumentException {
            switch (this) {
                case INTEGER:
                    switch (specifier) {
                        case "hh":
                            return CHAR;
                        case "h":
                            return SHORT;
                        case "ll":
                            return LONGLONG;
                        case "j":
                            return INTMAX_T;
                        case "z":
                            return SIZE_T;
                        case "t":
                            return PTRDIFF_T;
                        case "l":
                            return LONG;
                        case "":
                            return INT;
                    }
                    break;
                case CHARACTER:
                    switch (specifier) {
                        case "l":
                            return WCHAR_T;
                        case "":
                            return INT;
                    }
                    break;
                case FLOAT:
                    switch (specifier) {
                        case "l":
                            return DOUBLE;
                        case "L":
                            return LONGDOUBLE;
                        case "":
                            return LengthSpecifier.FLOAT;
                    }
                    break;
                case POINTER:
                    if (specifier.equals("")) {
                        return PTR;
                    }
                    break;
                case STRING:
                case RAW:
                    if (specifier.equals("")) {
                        return null;
                    }
            }
            throw new IllegalArgumentException("Unexpected value: " + specifier);
        }
    }

}
