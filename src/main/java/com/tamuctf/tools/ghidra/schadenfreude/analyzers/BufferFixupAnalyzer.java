/* ###
 * IP: GHIDRA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tamuctf.tools.ghidra.schadenfreude.analyzers;

import com.tamuctf.tools.ghidra.schadenfreude.decomp.SchadenfreudeDecompiler;
import com.tamuctf.tools.ghidra.schadenfreude.util.ProgramUtils;
import com.tamuctf.tools.ghidra.schadenfreude.util.TaskMonitorWrapper;
import ghidra.app.decompiler.DecompileResults;
import ghidra.app.services.AbstractAnalyzer;
import ghidra.app.services.AnalysisPriority;
import ghidra.app.services.AnalyzerType;
import ghidra.app.util.importer.MessageLog;
import ghidra.framework.options.Options;
import ghidra.program.model.address.AddressSetView;
import ghidra.program.model.data.*;
import ghidra.program.model.lang.Register;
import ghidra.program.model.listing.*;
import ghidra.program.model.pcode.*;
import ghidra.program.model.symbol.SourceType;
import ghidra.util.Msg;
import ghidra.util.exception.DuplicateNameException;
import ghidra.util.exception.InvalidInputException;
import ghidra.util.task.CancelledListener;
import ghidra.util.task.TaskMonitor;
import org.apache.commons.lang3.tuple.Pair;

import java.util.*;
import java.util.AbstractMap.SimpleEntry;
import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * TODO: Provide class-level documentation that describes what this analyzer
 * does.
 */
public class BufferFixupAnalyzer extends AbstractAnalyzer {

    public static final AnalysisPriority PRIORITY = AnalysisPriority.LOW_PRIORITY;
    private static final String ANALYZER_NAME = "Buffer Fixup";
    private static final String ANALYZER_DESCRIPTION = "A fixer for those pesky stack buffers";
    private static final String TRAVERSE_OPTION = "Traverse function calls?";
    private static final boolean TRAVERSE_DEFAULT = true;
    private static final String TRAVERSE_DESCRIPTION = "Traverse through function calls to determine types more precisely based on how they're used";
    private static final String TIMEOUT_OPTION = "Decompiler Timeout";
    private static final int TIMEOUT_DEFAULT = 3;
    private static final String TIMEOUT_DESCRIPTION = "Number of seconds before the decompiler times out";
    private static final String CREATE_OPTION = "Create Variables";
    private static final boolean CREATE_DEFAULT = true;
    private static final String CREATE_DESCRIPTION = "Allow fixups to include the creation of new variables";
    private final SchadenfreudeDecompiler decomp;
    private int timeout;
    private boolean traverse;
    private boolean create;

    public BufferFixupAnalyzer() {
        super(ANALYZER_NAME, ANALYZER_DESCRIPTION, AnalyzerType.FUNCTION_ANALYZER);
        this.setSupportsOneTimeAnalysis();
        this.setPriority(PRIORITY);
        traverse = TRAVERSE_DEFAULT;
        timeout = TIMEOUT_DEFAULT;
        create = CREATE_DEFAULT;
        decomp = SchadenfreudeDecompiler.getInstance();
    }

    @Override
    public boolean getDefaultEnablement(Program program) {
        return true;
    }

    @Override
    public boolean canAnalyze(Program program) {
        return program.getLanguage().supportsPcode();
    }

    @Override
    public void optionsChanged(Options options, Program program) {
        timeout = options.getInt(TIMEOUT_OPTION, TIMEOUT_DEFAULT);
        traverse = options.getBoolean(TRAVERSE_OPTION, TRAVERSE_DEFAULT);
        create = options.getBoolean(CREATE_OPTION, CREATE_DEFAULT);
    }

    @Override
    public void registerOptions(Options options, Program program) {
        options.registerOption(TIMEOUT_OPTION, TIMEOUT_DEFAULT, null, TIMEOUT_DESCRIPTION);
        options.registerOption(TRAVERSE_OPTION, TRAVERSE_DEFAULT, null, TRAVERSE_DESCRIPTION);
        options.registerOption(CREATE_OPTION, CREATE_DEFAULT, null, CREATE_DESCRIPTION);
    }

    @Override
    public boolean added(Program program, AddressSetView set, TaskMonitor monitor, MessageLog log) {
        decomp.openProgram(program);
        decomp.flushCaches();

        List<Function> functions = StreamSupport
                .stream(program.getFunctionManager().getFunctions(set, true).spliterator(), false)
                .filter(f -> !f.isThunk()) // no point
                .collect(Collectors.toUnmodifiableList());

        if (functions.isEmpty()) {
            return false;
        }

        Register stackRegister = ProgramUtils.getStackRegister(program);

        try {
            Map<HighFunction, List<PcodeOp>> pcodes = decompileFunctions(functions, monitor);
            if (pcodes == null) {
                return false;
            }
            TaskMonitor wrapped = new TaskMonitorWrapper(monitor);
            Map<HighFunction, List<PcodeOp>> filtered = new HashMap<>();
            Map<HighFunction, Set<HighVariable>> interesting = new HashMap<>();
            pcodes.forEach((hf, pcode) -> filtered.put(hf, pcode.stream().filter(op -> {
                List<HighVariable> curr = this.onlyInteresting(hf, stackRegister, op);
                if (!curr.isEmpty()) {
                    interesting.computeIfAbsent(hf, _hf -> new HashSet<>()).addAll(curr);
                    return true;
                }
                return false;
            }).collect(Collectors.toUnmodifiableList())));
            Map<HighFunction, List<HighVariable>> offsetMap = filtered.entrySet().stream().map(p -> {
                HighFunction hf = p.getKey();
                Set<Integer> offsets = new TreeSet<>();
                Function function = p.getKey().getFunction();
                StackFrame frame = function.getStackFrame();
                int local_start = 0;
                int local_end = local_start - frame.getLocalSize();
                p.getValue().forEach(op -> {
                    int opcode = op.getOpcode();
                    if (opcode == PcodeOp.PTRSUB) {
                        int offset = (int) op.getInput(1).getOffset(); // this is okay, I promise
                        if (local_start > offset && offset > local_end) {
                            offsets.add(offset);
                        }
                    } else {
                        Stream.concat(Stream.of(op.getOutput()), Arrays.stream(op.getInputs())).filter(Objects::nonNull)
                                .map(Varnode::getHigh).filter(Objects::nonNull).map(HighVariable::getSymbol)
                                .filter(Objects::nonNull).filter(hv -> hv.getStorage().isStackStorage())
                                .map(hv -> hv.getStorage().getStackOffset())
                                .filter(offset -> local_start > offset && offset > local_end).forEach(offsets::add);
                    }
                });
                List<HighVariable> res = StreamSupport
                        .stream(((Iterable<HighSymbol>) () -> p.getKey().getLocalSymbolMap().getSymbols())
                                .spliterator(), false).map(HighSymbol::getHighVariable).filter(Objects::nonNull)
                        .filter(hv -> hv.getSymbol().getStorage().isStackStorage())
                        .filter(v -> offsets.contains(v.getSymbol().getStorage().getStackOffset()))
                        .collect(Collectors.toList());
                if (create) {
                    Set<Integer> remaining = new TreeSet<>(offsets);
                    List<Variable> existing = Arrays.stream(hf.getFunction().getAllVariables())
                            .filter(Variable::hasStackStorage).filter(v -> offsets.contains(v.getStackOffset()))
                            .peek(v -> remaining.remove(v.getStackOffset())).collect(Collectors.toList());
                    remaining.removeAll(existing.stream().map(Variable::getStackOffset).collect(Collectors.toSet()));
                    if (!offsets.isEmpty()) {
                        int epOffset = -p.getKey().getFunctionPrototype().getExtraPop();
                        for (int offset : remaining) {
                            int nextOffset = offsets.stream().filter(other -> offset < other).findAny()
                                    .orElse(epOffset);
                            int size = nextOffset - offset;
                            DataType type = Undefined1DataType.dataType;
                            int count = size / type.getLength();
                            String name = String.format("local_%s", Integer.toHexString(-offset));
                            try {
                                Variable variable = new LocalVariableImpl(name,
                                        new ArrayDataType(type, count, type.getLength()), offset, program);
                                function.addLocalVariable(variable, SourceType.ANALYSIS);
                                Msg.info(BufferFixupAnalyzer.class,
                                        String.format("Added %s to %s", name, function.getName()));
                            } catch (InvalidInputException | DuplicateNameException e) {
                                Msg.warn(BufferFixupAnalyzer.class, "Unable to add new variable " + name, e);
                            }
                        }
                        Map<HighFunction, List<PcodeOp>> newPcodes = decompileFunctions(
                                Collections.singletonList(function), monitor);
                        if (newPcodes != null) {
                            Optional<Map.Entry<HighFunction, List<PcodeOp>>> entry = newPcodes.entrySet().stream()
                                    .findAny();
                            if (entry.isPresent()) {
                                HighFunction newHigh = entry.get().getKey();
                                List<PcodeOp> pcode = entry.get().getValue();
                                hf = newHigh;
                                res = StreamSupport
                                        .stream(((Iterable<HighSymbol>) () -> newHigh.getLocalSymbolMap().getSymbols())
                                                .spliterator(), false).map(HighSymbol::getHighVariable)
                                        .filter(Objects::nonNull)
                                        .filter(hv -> hv.getSymbol().getStorage().isStackStorage())
                                        .filter(v -> offsets.contains(v.getSymbol().getStorage().getStackOffset()))
                                        .collect(Collectors.toList());
                                Set<HighVariable> newInteresting = new HashSet<>();
                                pcodes.put(newHigh, pcode);
                                interesting.put(hf, newInteresting);
                                filtered.put(hf, pcode.stream().filter(op -> {
                                    List<HighVariable> curr = this.onlyInteresting(newHigh, stackRegister, op);
                                    if (!curr.isEmpty()) {
                                        newInteresting.addAll(curr);
                                        return true;
                                    }
                                    return false;
                                }).collect(Collectors.toUnmodifiableList()));
                            }
                        }
                    }
                }
                return Pair.of(hf, res);
            }).collect(Collectors.toMap(Pair::getLeft, Pair::getRight));
            filtered.forEach((hf, ops) -> {
                int epOffset = -hf.getFunctionPrototype().getExtraPop();
                List<HighVariable> offsets = offsetMap.get(hf);
                Set<HighVariable> hmm = interesting.get(hf);
                Map<HighVariable, List<DataType>> datatypes = new HashMap<>();
                ops.stream().filter(op -> op.getOpcode() == PcodeOp.PTRSUB) // only the pointers
                        .forEach(pcode -> {
                            Optional<HighVariable> maybeVar = offsets.stream()
                                    .filter(v -> v.getSymbol().getStorage().getStackOffset() ==
                                            pcode.getInput(1).getOffset()).findAny();
                            if (maybeVar.isPresent()) {
                                HighVariable var = maybeVar.get();
                                List<DataType> types = datatypes.computeIfAbsent(var, v -> new LinkedList<>());
                                Varnode output = pcode.getOutput();
                                discoverTypes(output, wrapped).stream()
                                        .filter(o -> o instanceof PointerDataType || o instanceof Array)
                                        .forEach(types::add);
                            }
                        });
                Map<HighVariable, Map<DataType, Long>> counted = datatypes.entrySet().stream()
                        .map(e -> new AbstractMap.SimpleEntry<>(e.getKey(), e.getValue().stream().collect(
                                Collectors.groupingBy(java.util.function.Function.identity(), Collectors.counting()))))
                        .collect(Collectors.toMap(SimpleEntry::getKey, SimpleEntry::getValue));
                List<HighVariable> all = StreamSupport
                        .stream(((Iterable<HighSymbol>) () -> hf.getLocalSymbolMap().getSymbols()).spliterator(), false)
                        .map(HighSymbol::getHighVariable).filter(Objects::nonNull)
                        .filter(hv -> hv.getSymbol().getStorage().isStackStorage())
                        .sorted(Comparator.comparingInt(hv -> hv.getSymbol().getStorage().getStackOffset())).distinct()
                        .collect(Collectors.toList());
                counted.forEach((v, dts) -> {
                    int offset = v.getSymbol().getStorage().getStackOffset();
                    int nextOffset;
                    {
                        ListIterator<HighVariable> hvIter = all.listIterator();
                        HighVariable consumed = hvIter.next();
                        while (hvIter.hasNext() && consumed.getSymbol().getStorage().getStackOffset() <
                                v.getSymbol().getStorage().getStackOffset()) {
                            consumed = hvIter.next();
                        }
                        int believedNext = consumed.getSymbol().getStorage().getStackOffset() + consumed.getSize();
                        for (; hvIter.hasNext(); believedNext =
                                consumed.getSymbol().getStorage().getStackOffset() + consumed.getSize()) {
                            consumed = hvIter.next();
                            final int finBN = believedNext;
                            if (consumed.getSymbol().getStorage().getStackOffset() > believedNext || hmm.stream()
                                    .anyMatch(var -> var.getSymbol().getStorage().getStackOffset() == finBN)) {
                                break;
                            }
                        }
                        if (believedNext >= epOffset) {
                            believedNext = epOffset;
                        }
                        nextOffset = believedNext;
                    }
                    int size = nextOffset - offset;
                    List<SimpleEntry<DataType, Integer>> preferredSized = dts.entrySet().stream()
                            .sorted((e1, e2) -> e2.getValue().compareTo(e1.getValue())) // inverted
                            .map(e -> {
                                DataType wrapperType = e.getKey();
                                if (wrapperType instanceof Array) {
                                    return new AbstractMap.SimpleEntry<>(((Array) wrapperType).getDataType(),
                                            size / ((Array) wrapperType).getDataType().getLength());
                                } else if (wrapperType instanceof PointerDataType) {
                                    return new AbstractMap.SimpleEntry<>(((PointerDataType) wrapperType).getDataType(),
                                            size / ((PointerDataType) wrapperType).getDataType().getLength());
                                }
                                throw new IllegalStateException("DataType provided was not an array or pointer.");
                            }).filter(e -> e.getValue() > 1).sorted((e1, e2) -> {
                                if (e1.getKey() instanceof Undefined) {
                                    if (e2.getKey() instanceof Undefined) {
                                        return 0;
                                    }
                                    return 1;
                                } else if (e2.getKey() instanceof Undefined) {
                                    return -1;
                                }
                                return 0;
                            }).collect(Collectors.toUnmodifiableList());
                    if (!preferredSized.isEmpty()) {
                        SimpleEntry<DataType, Integer> best = preferredSized.get(0);
                        DataType suggested = program.getDataTypeManager().addDataType(
                                new ArrayDataType(best.getKey(), best.getValue(), best.getKey().getLength()),
                                DataTypeConflictHandler.KEEP_HANDLER);
                        Arrays.stream(hf.getFunction().getVariables(VariableFilter.STACK_VARIABLE_FILTER))
                                .filter(var -> var.getStackOffset() == offset).findAny().ifPresent(var -> {
                            try {
                                var.setDataType(suggested, false, true, SourceType.ANALYSIS);
                            } catch (InvalidInputException e3) {
                                e3.printStackTrace();
                            }
                        });
                    }
                });
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    private Map<HighFunction, List<PcodeOp>> decompileFunctions(List<Function> functions, TaskMonitor monitor) {
        CountDownLatch latch = new CountDownLatch(functions.size());

        monitor.initialize(functions.size());
        monitor.setIndeterminate(false);
        monitor.setMessage("Decomipiling " + functions.size() + " functions");

        Map<HighFunction, List<PcodeOp>> results = new ConcurrentHashMap<>();
        for (Function function : functions) {
            decomp.decompileFunction(function, timeout, monitor, res -> {
                if (res.decompileCompleted()) {
                    LinkedList<PcodeOp> ops = new LinkedList<>();
                    HighFunction high = res.getHighFunction();
                    high.getPcodeOps().forEachRemaining(ops::add);
                    results.put(high, ops);
                }
                monitor.incrementProgress(1);
                latch.countDown();
            });
        }

        Thread currentThread = Thread.currentThread();
        CancelledListener listener = currentThread::interrupt;
        try {
            monitor.addCancelledListener(listener);
            latch.await();
        } catch (InterruptedException e) {
            return null;
        } finally {
            monitor.removeCancelledListener(listener);
        }

        return results;
    }

    private List<HighVariable> onlyInteresting(HighFunction hf, Register stackRegister, PcodeOp pcode) {
        switch (pcode.getOpcode()) {
            case PcodeOp.INDIRECT:
                return Collections.emptyList();
            case PcodeOp.COPY:
                if (pcode.getOutput().getHigh().getSymbol() != null &&
                        pcode.getOutput().getHigh().getSymbol().getStorage().isStackStorage() &&
                        !pcode.getInput(0).isConstant()) {
                    return Collections.singletonList(pcode.getOutput().getHigh());
                }
                return Collections.emptyList();
            case PcodeOp.PTRSUB:
                if (pcode.getInput(0).isRegister() && stackRegister
                        .contains(hf.getFunction().getProgram().getRegister(pcode.getInput(0).getAddress())) &&
                        pcode.getInput(1).isConstant()) {
                    List<HighVariable> interesting = new LinkedList<>();
                    hf.getLocalSymbolMap().getSymbols().forEachRemaining(hs -> {
                        if (hs.getHighVariable() != null) {
                            for (Varnode vn : hs.getHighVariable().getInstances()) {
                                if (vn.getAddress().isStackAddress() &&
                                        vn.getOffset() == pcode.getInput(1).getOffset()) {
                                    HighVariable high = vn.getHigh();
                                    if (high != null) {
                                        interesting.add(high);
                                    }
                                }
                            }
                        }
                    });
                    return interesting;
                }
                return Collections.emptyList();
            default: // EXTREMELY false positive prone
                return Stream.concat(Stream.of(pcode.getOutput()), Arrays.stream(pcode.getInputs()))
                        .filter(Objects::nonNull).filter(vn -> vn.getAddress().isStackAddress()).map(Varnode::getHigh)
                        .filter(Objects::nonNull).collect(Collectors.toList());
        }
    }

    private List<DataType> discoverTypes(Varnode node, TaskMonitor monitor) {
        Program program = node.getHigh().getHighFunction().getFunction().getProgram();
        List<DataType> types = new LinkedList<>();
        List<Varnode> allNodes = new LinkedList<>();
        Queue<Varnode> nodes = new LinkedBlockingQueue<>();
        nodes.add(node);
        while (!nodes.isEmpty()) {
            node = nodes.poll();
            allNodes.add(node);
            HighVariable high = node.getHigh();
            if (high != null) {
                DataType type = high.getDataType();
                if (type != null) {
                    types.add(type);
                }
            }
            Iterator<PcodeOp> descendants = node.getDescendants();
            if (descendants != null) {
                final Varnode finNode = node;
                descendants.forEachRemaining(op -> {
                    switch (op.getOpcode()) {
                        case PcodeOp.CAST:
                        case PcodeOp.INT_ADD:
                        case PcodeOp.INT_SUB:
                        case PcodeOp.PTRSUB:
                            if (!allNodes.contains(op.getOutput())) {
                                nodes.add(op.getOutput());
                            }
                            break;
                        case PcodeOp.CALL:
                            int slot = -1;
                            for (int i = 1; i < op.getInputs().length; ++i) {
                                if (op.getInputs()[i].equals(finNode)) {
                                    slot = i - 1; // offset due to call being the 0th argument
                                    break;
                                }
                            }
                            if (slot != -1) {
                                Function called = program.getFunctionManager()
                                        .getFunctionContaining(op.getInput(0).getAddress());
                                if (called.isThunk()) {
                                    called = called.getThunkedFunction(true);
                                }
                                if (called.isExternal() || !traverse) {
                                    if (slot < called.getParameterCount()) { // TODO support kwargs
                                        types.add(called.getParameter(slot).getDataType());
                                    }
                                    break;
                                } else {
                                    Future<DecompileResults> decompileResultsFuture = decomp
                                            .decompileFunction(called, timeout, monitor);
                                    try {
                                        DecompileResults decompileResults = Objects
                                                .requireNonNull(decompileResultsFuture).get();
                                        // don't handle the alternative, low priority
                                        if (decompileResults.decompileCompleted()) {
                                            HighFunction hf = decompileResults.getHighFunction();
                                            if (slot < hf.getLocalSymbolMap().getNumParams()) { // TODO support kwargs
                                                nodes.add(hf.getLocalSymbolMap().getParam(slot).getRepresentative());
                                            }
                                        }
                                    } catch (InterruptedException | ExecutionException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                    }
                });
            }
        }
        return types;
    }

}
