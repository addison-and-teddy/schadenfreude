#if _FORTIFY_SOURCE > 0
  #undef _FORTIFY_SOURCE
  #define _FORTIFY_SOURCE 0
#endif

#include <stdio.h>
#include <string.h>

void gets_vuln(){
  char __attribute__((no_reorder)) buf[16];
  memset(buf, 0, 16);
  gets(buf);
  puts(buf);
}
