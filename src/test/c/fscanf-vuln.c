#if _FORTIFY_SOURCE > 0
  #undef _FORTIFY_SOURCE
  #define _FORTIFY_SOURCE 0
#endif

#include <stdio.h>
#include <string.h>

#warning fscanf is not implemented.
void fscanf_vuln(){}
