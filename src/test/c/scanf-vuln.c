#if _FORTIFY_SOURCE > 0
  #undef _FORTIFY_SOURCE
  #define _FORTIFY_SOURCE 0
#endif

#include <stdio.h>
#include <string.h>
#ifdef __aarch64__
#define PARTIAL_SMASH_DEPTH_1 "33"
#define PARTIAL_SMASH_DEPTH_2 "17"
#define COMPLETE_SMASH_DEPTH_1 "40"
#define COMPLETE_SMASH_DEPTH_2 "24"
#endif
#ifdef __x86_64__
#define PARTIAL_SMASH_DEPTH_1 "57"
#define PARTIAL_SMASH_DEPTH_2 "41"
#define COMPLETE_SMASH_DEPTH_1 "64"
#define COMPLETE_SMASH_DEPTH_2 "48"
#endif

#ifndef PARTIAL_SMASH_DEPTH_1
#error PARTIAL_SMASH_DEPTH_1 was not defined for this architecture.
#else
void scanf_vuln(){
  char __attribute__((no_reorder)) buf2[16];
  char __attribute__((no_reorder)) buf1[16];
  memset(buf1, 0, 16);
  memset(buf2, 0, 16);
  scanf("%16s", buf1); // no error
  puts(buf1);
  scanf("%17s", buf1); // buffer overrun
  puts(buf1);
  scanf("%" PARTIAL_SMASH_DEPTH_1 "s", buf1); // partial smash
  puts(buf1);
  scanf("%" COMPLETE_SMASH_DEPTH_1 "s", buf1); // complete smash
  puts(buf1);
  scanf("%s", buf1); // complete smash, unsized
  puts(buf1);
  scanf("%" PARTIAL_SMASH_DEPTH_2 "s", buf2); // partial smash
  puts(buf2);
  scanf("%" COMPLETE_SMASH_DEPTH_2 "s", buf2); // complete smash
  puts(buf2);
  scanf("%s", buf2); // complete smash, unsized
  puts(buf1);
}
#endif