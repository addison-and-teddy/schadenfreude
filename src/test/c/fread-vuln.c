#if _FORTIFY_SOURCE > 0
  #undef _FORTIFY_SOURCE
  #define _FORTIFY_SOURCE 0
#endif

#include <stdio.h>
#include <string.h>

#ifdef __aarch64__
#define STACK_DEPTH 32
#endif
#ifdef __x86_64__
#define STACK_DEPTH 56
#endif

#ifndef STACK_DEPTH
#error STACK_DEPTH was not defined for this architecture.
#else
void fread_vuln(){
  char __attribute__((no_reorder)) buf2[16];
  char __attribute__((no_reorder)) buf1[16];
  memset(buf1, 0, 16);
  memset(buf2, 0, 16);
  fread(buf1, 1, sizeof(buf1), stdin); // no error
  puts(buf1);
  fread(buf1, 1, sizeof(buf1) + 1, stdin); // overrun
  puts(buf1);
  fread(buf1, 1, STACK_DEPTH + 1, stdin); // partial smash
  puts(buf1);
  fread(buf1, 1, STACK_DEPTH + 8, stdin); // complete smash
  puts(buf1);
  fread(buf2, 1, STACK_DEPTH - sizeof(buf2) + 1, stdin); // partial smash
  puts(buf2);
  fread(buf2, 1, STACK_DEPTH - sizeof(buf2) + 8, stdin); // complete smash
  puts(buf2);
}
#endif
