#if _FORTIFY_SOURCE > 0
  #undef _FORTIFY_SOURCE
  #define _FORTIFY_SOURCE 0
#endif

#include <stdio.h>
#include <string.h>

#ifdef __aarch64__
#define STACK_DEPTH sizeof(buf1)
#endif
#ifdef __x86_64__
#define STACK_DEPTH 8 + sizeof(buf1)
#endif

#define BUF_SIZE 16

#ifndef STACK_DEPTH
#error STACK_DEPTH was not defined for this architecture.
#else
void fgets_vuln_through_2(char *buf1, int size) {
  fgets(buf1, size, stdin);
  puts(buf1);
}

void fgets_vuln_caller_2(){
  char __attribute__((no_reorder)) buf1[BUF_SIZE];
  memset(buf1, 0, 16);
  fgets_vuln_through_2(buf1, sizeof(buf1)); // no error
  fgets_vuln_through_2(buf1, STACK_DEPTH + 1); // partial smash
  fgets_vuln_through_2(buf1, STACK_DEPTH + 8); // complete smash
}
#endif
