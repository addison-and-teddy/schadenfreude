# The C Test Code

The sources provided in this folder are designed to test various analysers implemented by Schadenfreude to confirm
correct operation. As they are binary analysers, we need binaries to test. These are their sources.

*law and order sound effect plays*

## Design

These are very simple source files. They are compiled into individual object files, then assembled into a shared object
library. If any single function fails to compile for a given architecture, the make process fails.

Let's walk through [`fgets-vuln.c`](fgets-vuln.c) to get a good understanding of the different problems we need to solve
to make this all work:

```c
#if _FORTIFY_SOURCE > 0
  #undef _FORTIFY_SOURCE
  #define _FORTIFY_SOURCE 0
#endif

#include <stdio.h>
#include <string.h>

#ifdef __aarch64__
#define STACK_DEPTH 32
#endif
#ifdef __x86_64__
#define STACK_DEPTH 56
#endif

#ifndef STACK_DEPTH
#error STACK_DEPTH was not defined for this architecture.
#else
void fgets_vuln(){
  char __attribute__((no_reorder)) buf2[16];
  char __attribute__((no_reorder)) buf1[16];
  memset(buf1, 0, 16);
  memset(buf2, 0, 16);
  fgets(buf1, sizeof(buf1), stdin); // no error
  puts(buf1);
  fgets(buf1, sizeof(buf1) + 1, stdin); // overrun
  puts(buf1);
  fgets(buf1, STACK_DEPTH + 1, stdin); // partial smash
  puts(buf1);
  fgets(buf1, STACK_DEPTH + 8, stdin); // complete smash
  puts(buf1);
  fgets(buf2, STACK_DEPTH - sizeof(buf1) + 1, stdin); // partial smash
  puts(buf2);
  fgets(buf2, STACK_DEPTH - sizeof(buf1) + 8, stdin); // complete smash
  puts(buf2);
}
#endif
```

### Disabling source fortifications

A lot of C functions employ compilation-time checks for overflows and "fix" your code for you. We don't want that, so we
disable it:

```c
#if _FORTIFY_SOURCE > 0
  #undef _FORTIFY_SOURCE
  #define _FORTIFY_SOURCE 0
#endif
```

### Multi-architectural support

We need to ensure that the algorithms we employ work generically so we don't have to re-implement for every processor.
Thus, stack depth information is provided at the start of each source file to make sure we hit the right stack member.

```c
#ifdef __aarch64__
#define STACK_DEPTH 32
#endif
#ifdef __x86_64__
#define STACK_DEPTH 56
#endif
```

Additionally, the builder for the source code might use some processors we haven't implemented yet, so we'll fail fast
if the stack depth isn't defined rather than setting a default value:

```c
#ifndef STACK_DEPTH
#error STACK_DEPTH was not defined for this architecture.
#else
```

### Buffer arrangement

C moves buffers in the stack around a lot. We don't want that:

```c
  char __attribute__((no_reorder)) buf2[16];
  char __attribute__((no_reorder)) buf1[16];
```

