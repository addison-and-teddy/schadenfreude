#if _FORTIFY_SOURCE > 0
  #undef _FORTIFY_SOURCE
  #define _FORTIFY_SOURCE 0
#endif

#include <stdio.h>
#include <string.h>

#ifdef __aarch64__
#define STACK_DEPTH 32
#endif
#ifdef __x86_64__
#define STACK_DEPTH 40
#endif

#define BUF_SIZE 16

#ifndef STACK_DEPTH
#error STACK_DEPTH was not defined for this architecture.
#else
void fgets_vuln_through(char *buf1, char *buf2) {
  fgets(buf1, BUF_SIZE, stdin); // no error
  puts(buf1);
  fgets(buf1, BUF_SIZE + 1, stdin); // overrun
  puts(buf1);
  fgets(buf1, STACK_DEPTH + 1, stdin); // partial smash
  puts(buf1);
  fgets(buf1, STACK_DEPTH + 8, stdin); // complete smash
  puts(buf1);
  fgets(buf2, STACK_DEPTH - BUF_SIZE + 1, stdin); // partial smash
  puts(buf2);
  fgets(buf2, STACK_DEPTH - BUF_SIZE + 8, stdin); // complete smash
  puts(buf2);
}

void fgets_vuln_caller(){
  char __attribute__((no_reorder)) buf2[BUF_SIZE];
  char __attribute__((no_reorder)) buf1[BUF_SIZE];
  memset(buf1, 0, 16);
  memset(buf2, 0, 16);
  fgets_vuln_through(buf1, buf2);
}
#endif
