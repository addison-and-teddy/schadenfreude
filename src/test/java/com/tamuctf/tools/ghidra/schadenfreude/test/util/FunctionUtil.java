package com.tamuctf.tools.ghidra.schadenfreude.test.util;

import ghidra.program.model.listing.Function;
import ghidra.program.model.listing.Program;
import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.stream.StreamSupport;

@UtilityClass
public class FunctionUtil {

    public @Nullable Function getFunctionByName(@NotNull Program program, @NotNull String name) {
        return StreamSupport.stream(program.getFunctionManager().getFunctions(true).spliterator(), false)
                .filter(function -> !function.isThunk()).filter(f -> f.getName().equals(name)).findAny().orElse(null);
    }

}
