package com.tamuctf.tools.ghidra.schadenfreude.strings;

import com.tamuctf.tools.ghidra.schadenfreude.strings.format.ScanfEntry;
import org.apache.commons.lang3.tuple.Pair;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.Arrays;
import java.util.List;

import static com.tamuctf.tools.ghidra.schadenfreude.strings.ScanfParser.parseFormatString;
import static com.tamuctf.tools.ghidra.schadenfreude.strings.format.ScanfEntry.Type.*;
import static org.junit.jupiter.api.Assertions.*;

class ScanfParserTest {

    @Contract(pure = true)
    public static @NotNull List<Pair<String, ScanfEntry.Type>> getAllTypes() {
        //@formatter:off
        return Arrays.asList(
                Pair.of("%i", INTEGER),
                Pair.of("%d", INTEGER),
                Pair.of("%u", INTEGER),
                Pair.of("%o", INTEGER),
                Pair.of("%x", INTEGER),
                Pair.of("%n", INTEGER),
                Pair.of("%f", FLOAT),
                Pair.of("%e", FLOAT),
                Pair.of("%g", FLOAT),
                Pair.of("%a", FLOAT),
                Pair.of("%c", CHARACTER),
                Pair.of("%s", STRING),
                Pair.of("%p", POINTER),
                Pair.of("%%", RAW)
        );
        //@formatter:on
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "",
            "a valid string without any formatting",
            "with newline\n%s in between",
            "some\twhitespace\n",
            "%23hhx             ",
            "  %5Le  "
    })
    void parseFormatStringTest(String valid) {
        parseFormatString(valid);
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "An incomplete format string %", "invalid pairing %hp", "negative size %-1n", "%5", "%hh", "%n%"
    })
    void parseFormatStringFailuresTest(String valid) {
        assertThrows(IllegalArgumentException.class, () -> parseFormatString(valid));
    }

    @ParameterizedTest
    @MethodSource("com.tamuctf.tools.ghidra.schadenfreude.strings.ScanfParserTest#getAllTypes")
    void parseFormatStringTypeCorrectness(Pair<String, ScanfEntry.Type> typePair) {
        assertEquals(typePair.getValue(), parseFormatString(typePair.getKey()).getEntries()[0].getType());
    }

    @Test
    void parseFormatStringSeesIgnored() {
        ScanfParser.ScanfFormatString fs = parseFormatString("%s%*s");
        assertFalse(fs.getEntries()[0].isIgnored());
        assertTrue(fs.getEntries()[1].isIgnored());
    }

}