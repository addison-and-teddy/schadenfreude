package com.tamuctf.tools.ghidra.schadenfreude.analyzers;

import com.tamuctf.tools.ghidra.schadenfreude.test.util.FunctionUtil;
import ghidra.program.model.data.Array;
import ghidra.program.model.data.CharDataType;
import ghidra.program.model.listing.Function;
import ghidra.program.model.listing.Program;
import ghidra.program.model.listing.Variable;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.params.ParameterizedTest.DEFAULT_DISPLAY_NAME;

public class StackSmashTest extends AnalyzerTest {

    @ParameterizedTest(name = "{displayName}" + DEFAULT_DISPLAY_NAME)
    @MethodSource("com.tamuctf.tools.ghidra.schadenfreude.SchadenfreudeTestSuite#getPrograms")
    public void detectsGetsVuln(@NotNull Program program) {
        detects(program, "gets_vuln", v -> v.getLength() == 16 && v.getDataType() instanceof Array &&
                        ((Array) v.getDataType()).getDataType().isEquivalent(CharDataType.dataType), 1,
                Stream.of("^Stack smashed at [0-9a-f]{8} \\(by gets\\) \\(\\+0,16,[0-9]{2,}\\)$")
                        .map(Pattern::compile));
    }

    @ParameterizedTest(name = "{displayName}" + DEFAULT_DISPLAY_NAME)
    @MethodSource("com.tamuctf.tools.ghidra.schadenfreude.SchadenfreudeTestSuite#getPrograms")
    public void detectsFgetsVuln(@NotNull Program program) {
        detects(program, "fgets_vuln", v -> v.getLength() == 16 && v.getDataType() instanceof Array &&
                ((Array) v.getDataType()).getDataType().isEquivalent(CharDataType.dataType), 5, Stream.of(
                "^Buffer overrun of local_[0-9]{2} at [0-9a-f]{8} \\(by fgets, size 17\\) \\(\\+0,16,[0-9]{2,}\\)$",
                "^Stack partially smashed at [0-9a-f]{8} \\(by fgets, size [0-9]{2,}\\) \\(\\+0,16,[0-9]{2,}\\)$",
                "^Stack smashed at [0-9a-f]{8} \\(by fgets, size [0-9]{2,}\\) \\(\\+0,16,[0-9]{2,}\\)$",
                "^Stack partially smashed at [0-9a-f]{8} \\(by fgets, size [0-9]{2,}\\) \\(\\+0,16,[0-9]{2,}\\)$",
                "^Stack smashed at [0-9a-f]{8} \\(by fgets, size [0-9]{2,}\\) \\(\\+0,16,[0-9]{2,}\\)$")
                .map(Pattern::compile));
    }

    @ParameterizedTest(name = "{displayName}" + DEFAULT_DISPLAY_NAME)
    @MethodSource("com.tamuctf.tools.ghidra.schadenfreude.SchadenfreudeTestSuite#getPrograms")
    public void detectsFgetsVulnCaller(@NotNull Program program) {
        detects(program, "fgets_vuln_caller", v -> v.getLength() == 16 && v.getDataType() instanceof Array &&
                ((Array) v.getDataType()).getDataType().isEquivalent(CharDataType.dataType), 5, Stream.of(
                "^Buffer overrun of local_[0-9]{2} at [0-9a-f]{8} \\(by fgets, size 17\\) \\(\\+0,16,[0-9]{2,}\\)$",
                "^Stack partially smashed at [0-9a-f]{8} \\(by fgets, size [0-9]{2,}\\) \\(\\+0,16,[0-9]{2,}\\)$",
                "^Stack smashed at [0-9a-f]{8} \\(by fgets, size [0-9]{2,}\\) \\(\\+0,16,[0-9]{2,}\\)$",
                "^Stack partially smashed at [0-9a-f]{8} \\(by fgets, size [0-9]{2,}\\) \\(\\+0,16,[0-9]{2,}\\)$",
                "^Stack smashed at [0-9a-f]{8} \\(by fgets, size [0-9]{2,}\\) \\(\\+0,16,[0-9]{2,}\\)$")
                .map(Pattern::compile));
    }

    @ParameterizedTest(name = "{displayName}" + DEFAULT_DISPLAY_NAME)
    @MethodSource("com.tamuctf.tools.ghidra.schadenfreude.SchadenfreudeTestSuite#getPrograms")
    public void detectsFgetsVulnCaller2(@NotNull Program program) {
        detects(program, "fgets_vuln_caller_2", v -> v.getLength() == 16 && v.getDataType() instanceof Array &&
                ((Array) v.getDataType()).getDataType().isEquivalent(CharDataType.dataType), 2, Stream.of(
                "^Stack partially smashed at [0-9a-f]{8} \\(by fgets, size [0-9]{2,}\\) \\(\\+0,16,[0-9]{2,}\\)$",
                "^Stack smashed at [0-9a-f]{8} \\(by fgets, size [0-9]{2,}\\) \\(\\+0,16,[0-9]{2,}\\)$")
                .map(Pattern::compile));
    }

    @ParameterizedTest(name = "{displayName}" + DEFAULT_DISPLAY_NAME)
    @MethodSource("com.tamuctf.tools.ghidra.schadenfreude.SchadenfreudeTestSuite#getPrograms")
    public void detectsFreadVuln(@NotNull Program program) {
        detects(program, "fread_vuln", v -> v.getLength() == 16 && v.getDataType() instanceof Array &&
                ((Array) v.getDataType()).getDataType().isEquivalent(CharDataType.dataType), 5, Stream.of(
                "^Buffer overrun of local_[0-9]{2} at [0-9a-f]{8} \\(by fread, size 17\\) \\(\\+0,16,[0-9]{2,}\\)$",
                "^Stack partially smashed at [0-9a-f]{8} \\(by fread, size [0-9]{2,}\\) \\(\\+0,16,[0-9]{2,}\\)$",
                "^Stack smashed at [0-9a-f]{8} \\(by fread, size [0-9]{2,}\\) \\(\\+0,16,[0-9]{2,}\\)$",
                "^Stack partially smashed at [0-9a-f]{8} \\(by fread, size [0-9]{2,}\\) \\(\\+0,16,[0-9]{2,}\\)$",
                "^Stack smashed at [0-9a-f]{8} \\(by fread, size [0-9]{2,}\\) \\(\\+0,16,[0-9]{2,}\\)$")
                .map(Pattern::compile));
    }

    @ParameterizedTest(name = "{displayName}" + DEFAULT_DISPLAY_NAME)
    @MethodSource("com.tamuctf.tools.ghidra.schadenfreude.SchadenfreudeTestSuite#getPrograms")
    public void detectsMemcpyVuln(@NotNull Program program) {
        detects(program, "memcpy_vuln", v -> v.getDataType() instanceof Array &&
                ((Array) v.getDataType()).getDataType().isEquivalent(CharDataType.dataType), 2, Stream.of(
                "^Stack partially smashed at [0-9a-f]{8} \\(by memcpy, size [0-9]{2,}\\) \\(\\+0,24,[0-9]{2,}\\)$",
                "^Stack smashed at [0-9a-f]{8} \\(by memcpy, size [0-9]{2,}\\) \\(\\+0,24,[0-9]{2,}\\)$")
                .map(Pattern::compile));
    }

    @ParameterizedTest(name = "{displayName}" + DEFAULT_DISPLAY_NAME)
    @MethodSource("com.tamuctf.tools.ghidra.schadenfreude.SchadenfreudeTestSuite#getPrograms")
    public void detectsMempcpyVuln(@NotNull Program program) {
        detects(program, "mempcpy_vuln", v -> v.getDataType() instanceof Array &&
                ((Array) v.getDataType()).getDataType().isEquivalent(CharDataType.dataType), 2, Stream.of(
                "^Stack partially smashed at [0-9a-f]{8} \\(by mempcpy, size [0-9]{2,}\\) \\(\\+0,16,[0-9]{2,}\\)$",
                "^Stack smashed at [0-9a-f]{8} \\(by mempcpy, size [0-9]{2,}\\) \\(\\+0,16,[0-9]{2,}\\)$")
                .map(Pattern::compile));
    }

    @ParameterizedTest(name = "{displayName}" + DEFAULT_DISPLAY_NAME)
    @MethodSource("com.tamuctf.tools.ghidra.schadenfreude.SchadenfreudeTestSuite#getPrograms")
    @Disabled("Somehow doesn't work after 9.2 update; investigating.")
    public void detectsMemccpyVuln(@NotNull Program program) {
        detects(program, "memccpy_vuln", v -> v.getDataType() instanceof Array &&
                ((Array) v.getDataType()).getDataType().isEquivalent(CharDataType.dataType), 2, Stream.of(
                "^Stack partially smashed at [0-9a-f]{8} \\(by memccpy, size [0-9]{2,}\\) \\(\\+0,24,[0-9]{2,}\\)$",
                "^Stack smashed at [0-9a-f]{8} \\(by memccpy, size [0-9]{2,}\\) \\(\\+0,24,[0-9]{2,}\\)$")
                .map(Pattern::compile));
    }

    @ParameterizedTest(name = "{displayName}" + DEFAULT_DISPLAY_NAME)
    @MethodSource("com.tamuctf.tools.ghidra.schadenfreude.SchadenfreudeTestSuite#getPrograms")
    public void detectsReadVuln(@NotNull Program program) {
        detects(program, "read_vuln", v -> v.getLength() == 16 && v.getDataType() instanceof Array &&
                ((Array) v.getDataType()).getDataType().isEquivalent(CharDataType.dataType), 5, Stream.of(
                "^Buffer overrun of local_[0-9]{2} at [0-9a-f]{8} \\(by read, size 17\\) \\(\\+0,16,[0-9]{2,}\\)$",
                "^Stack partially smashed at [0-9a-f]{8} \\(by read, size [0-9]{2,}\\) \\(\\+0,16,[0-9]{2,}\\)$",
                "^Stack smashed at [0-9a-f]{8} \\(by read, size [0-9]{2,}\\) \\(\\+0,16,[0-9]{2,}\\)$",
                "^Stack partially smashed at [0-9a-f]{8} \\(by read, size [0-9]{2,}\\) \\(\\+0,16,[0-9]{2,}\\)$",
                "^Stack smashed at [0-9a-f]{8} \\(by read, size [0-9]{2,}\\) \\(\\+0,16,[0-9]{2,}\\)$")
                .map(Pattern::compile));
    }

    @ParameterizedTest(name = "{displayName}" + DEFAULT_DISPLAY_NAME)
    @MethodSource("com.tamuctf.tools.ghidra.schadenfreude.SchadenfreudeTestSuite#getPrograms")
    @Disabled("scanf is not yet implemented")
    public void detectsScanfVuln(@NotNull Program program) {
        detects(program, "scanf_vuln", v -> v.getLength() == 16 && v.getDataType() instanceof Array &&
                ((Array) v.getDataType()).getDataType().isEquivalent(CharDataType.dataType), 7, Stream.of(
                "^Buffer overrun of local_[0-9]{2} at [0-9a-f]{8} \\(by scanf, size 17\\) \\(\\+0,16,[0-9]{2,}\\)$",
                "^Stack partially smashed at [0-9a-f]{8} \\(by scanf, size [0-9]{2,}\\) \\(\\+0,16,[0-9]{2,}\\)$",
                "^Stack smashed at [0-9a-f]{8} \\(by scanf, size [0-9]{2,}\\) \\(\\+0,16,[0-9]{2,}\\)$",
                "^Stack smashed at [0-9a-f]{8} \\(by scanf\\) \\(\\+0,16,[0-9]{2,}\\)$",
                "^Stack partially smashed at [0-9a-f]{8} \\(by scanf, size [0-9]{2,}\\) \\(\\+0,16,[0-9]{2,}\\)$",
                "^Stack smashed at [0-9a-f]{8} \\(by scanf, size [0-9]{2,}\\) \\(\\+0,16,[0-9]{2,}\\)$",
                "^Stack smashed at [0-9a-f]{8} \\(by scanf\\) \\(\\+0,16,[0-9]{2,}\\)$").map(Pattern::compile));
    }

    private void detects(@NotNull Program program, @NotNull String function,
            @NotNull Predicate<Variable> smashedVariables, int count, @NotNull Stream<Pattern> patterns) {
        Function target = FunctionUtil.getFunctionByName(program, function);
        assertNotNull(target);
        List<Variable> variables = Arrays.stream(target.getLocalVariables()).filter(smashedVariables)
                .collect(Collectors.toList());
                variables.forEach(System.out::println);
        List<String> comments = variables.stream().map(Variable::getComment).filter(Objects::nonNull)
                .flatMap(c -> Arrays.stream(c.split("\n"))).collect(Collectors.toList());
                comments.forEach(System.out::println);
        assertEquals(count, comments.size());
        patterns.forEach(pattern -> assertTrue(comments.stream().anyMatch(s -> pattern.matcher(s).matches())));
    }

}
