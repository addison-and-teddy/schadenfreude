package com.tamuctf.tools.ghidra.schadenfreude.test.application;

import generic.jar.ResourceFile;
import ghidra.GhidraJarApplicationLayout;
import ghidra.framework.GModule;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public class SchadenfreudeTestApplicationLayout extends GhidraJarApplicationLayout {
    public SchadenfreudeTestApplicationLayout() throws IOException {
    }

    @Override
    protected Map<String, GModule> findGhidraModules() throws IOException {
        Map<String, GModule> modules = new HashMap<>(super.findGhidraModules());
        GModule schadenfreude = new GModule(this.getApplicationRootDirs(),
                new ResourceFile(Paths.get("").toAbsolutePath().toString()));
        modules.put("Schadenfreude", schadenfreude);
        return modules;
    }

    @Override
    public boolean inSingleJarMode() {
        return false;
    }
}
