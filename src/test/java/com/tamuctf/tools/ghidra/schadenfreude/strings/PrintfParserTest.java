package com.tamuctf.tools.ghidra.schadenfreude.strings;

import com.tamuctf.tools.ghidra.schadenfreude.strings.format.PrintfEntry;
import org.apache.commons.lang3.tuple.Pair;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.Arrays;
import java.util.List;

import static com.tamuctf.tools.ghidra.schadenfreude.strings.PrintfParser.parseFormatString;
import static com.tamuctf.tools.ghidra.schadenfreude.strings.format.LengthSpecifier.SHORT;
import static com.tamuctf.tools.ghidra.schadenfreude.strings.format.PrintfEntry.Type.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class PrintfParserTest {

    @Contract(pure = true)
    public static @NotNull List<Pair<String, PrintfEntry.Type>> getAllTypes() {
        //@formatter:off
        return Arrays.asList(
                Pair.of("%i", INTEGER),
                Pair.of("%d", INTEGER),
                Pair.of("%u", INTEGER),
                Pair.of("%o", INTEGER),
                Pair.of("%x", INTEGER),
                Pair.of("%f", FLOAT),
                Pair.of("%e", FLOAT),
                Pair.of("%g", FLOAT),
                Pair.of("%a", FLOAT),
                Pair.of("%c", CHARACTER),
                Pair.of("%s", STRING),
                Pair.of("%p", POINTER),
                Pair.of("%n", WRITEIN),
                Pair.of("%%", RAW)
        );
        //@formatter:on
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "",
            "a valid string without any formatting",
            "with newline\n%s in between",
            "some\twhitespace\n",
            "%23hhx             ",
            "  %5Le  "
    })
    void parseFormatStringTest(String valid) {
        parseFormatString(valid);
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "An incomplete format string %", "invalid pairing %hp", "negative size %-1n", "%5", "%hh", "%n%"
    })
    void parseFormatStringFailuresTest(String valid) {
        assertThrows(IllegalArgumentException.class, () -> parseFormatString(valid));
    }

    @ParameterizedTest
    @MethodSource("com.tamuctf.tools.ghidra.schadenfreude.strings.PrintfParserTest#getAllTypes")
    void parseFormatStringTypeCorrectness(Pair<String, PrintfEntry.Type> typePair) {
        assertEquals(typePair.getValue(), parseFormatString(typePair.getKey()).getEntries()[0].getType());
    }

    @SuppressWarnings("OptionalGetWithoutIsPresent")
    @Test
    void parseBig() {
        PrintfParser.PrintfFormatString fs = parseFormatString("%1$*8$hn");
        PrintfEntry entry = fs.getEntries()[0];
        assertEquals(1, entry.getArgument().getAsLong());
        assertEquals(8, entry.getPrecision().getAsLong());
        assertEquals(SHORT, entry.getLength());
        assertEquals(WRITEIN, entry.getType());
    }

}