package com.tamuctf.tools.ghidra.schadenfreude;

import com.google.common.io.Files;
import com.tamuctf.tools.ghidra.schadenfreude.test.application.SchadenfreudeApplicationConfiguration;
import com.tamuctf.tools.ghidra.schadenfreude.test.application.SchadenfreudeTestApplicationLayout;
import ghidra.base.project.GhidraProject;
import ghidra.framework.Application;
import ghidra.program.model.listing.Program;
import ghidra.util.Msg;
import lombok.Getter;
import org.reflections.Reflections;
import org.reflections.scanners.ResourcesScanner;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class SchadenfreudeTestSuite {

    public static final @SuppressWarnings("UnstableApiUsage") File folder = Files.createTempDir();
    private static final @Getter List<File> programFiles;
    private static final GhidraProject project;
    private static final @Getter List<Program> programs;

    static {
        try {
            Reflections reflections = new Reflections("", new ResourcesScanner());
            List<File> mutProgramFiles = reflections.getResources(Pattern.compile(".*vulnerable\\.so")).stream()
                    .map(s -> new File(
                            Objects.requireNonNull(SchadenfreudeTestSuite.class.getClassLoader().getResource(s))
                                    .getFile())).collect(Collectors.toList());
            String projPath = folder.getAbsolutePath();
            String projName = "test-proj";
            Application.initializeApplication(new SchadenfreudeTestApplicationLayout(),
                    new SchadenfreudeApplicationConfiguration());
            project = GhidraProject.createProject(projPath, projName, true);
            List<Program> mutPrograms = new ArrayList<>(mutProgramFiles.size());
            mutProgramFiles.removeIf(programFile -> {
                try {
                    Msg.info(SchadenfreudeTestSuite.class, "Attempting to load " + programFile.getName());
                    Program program = project.importProgram(programFile);
                    project.analyze(program, false);
                    mutPrograms.add(program);
                    return false;
                } catch (Throwable t) {
                    Msg.warn(SchadenfreudeTestSuite.class,
                            String.format("Not adding %s because loading failed.", programFile.getName()), t);
                    return true;
                }
            });
            programFiles = Collections.unmodifiableList(mutProgramFiles);
            programs = Collections.unmodifiableList(mutPrograms);
        } catch (Throwable t) {
            throw new RuntimeException(t);
        }
    }

}
