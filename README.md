# Schadenfreude

The automated vulnerability discovery plugin you probably don't need.

## Purpose

Schadenfreude is a proof-of-concept Ghidra plugin which performs rudimentary data analysis to discover vulnerabilities
present in software. It is _not_ complete by any means, but it is (hopefully) sound.

## Goals

Schadenfreude will:
 - [x] Fix buffer sizes to accurately represent stack buffers present in functions
   - This is mostly good to go, with a few obvious bugs (such as unions, partial stack usage, etc)
 - [ ] Implement rudimentary checks for libc input functions doing naughty things (e.g. fgets, read, printf, scanf, etc)
   - About halfway done! Most of the rest of the implementations are going to be copy-paste inspired.
 - [ ] Evaluate runtime values based off of emulated PCode (yay!)

## Design

In a nutshell, think [angr](https://angr.io) but more specific, a little more weight on static program analysis and less
on emulation, and it's based on decompiled PCode, so it's whatever platform you want.

Most of the design decisions for Schadenfreude are based on data flow graph analysis techniques described in Principles 
of Program Analysis and in The Art of War: Offensive Techniques in Binary Analysis:

```bibtex
@inproceedings{shoshitaishvili2016state,
  title={{SoK: (State of) The Art of War: Offensive Techniques in Binary Analysis}},
  author={Shoshitaishvili, Yan and Wang, Ruoyu and Salls, Christopher and
          Stephens, Nick and Polino, Mario and Dutcher, Audrey and Grosen, John and
          Feng, Siji and Hauser, Christophe and Kruegel, Christopher and Vigna, Giovanni},
  booktitle={IEEE Symposium on Security and Privacy},
  year={2016}
}

@book{10.5555/1965094,
  author = {Nielson, Flemming and Nielson, Hanne R. and Hankin, Chris},
  title = {Principles of Program Analysis},
  year = {1999},
  isbn = {978-3-662-03811-6},
  publisher = {Springer Publishing Company, Incorporated}
}
```

(yes, that's bibtex, yes I'm lazy)
